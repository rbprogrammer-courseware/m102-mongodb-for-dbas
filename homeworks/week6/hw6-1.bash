#!/usr/bin/env bash

HEADER_COUNTER=0
function displayHeader() {
    HEADER_COUNTER=$((HEADER_COUNTER+1))
    echo
    echo '========================================'
    echo "${HEADER_COUNTER} => ${@}"
    echo
}

function echoerr() {
    echo 'ERROR:' $@ 1>&2
}

function sleeps() {
    seconds=$1
    echo -n "Sleeping for ${seconds} seconds "
    for i in $(seq 1 $seconds) ; do
        sleep 1
        if [[ "x$(( $i % 10))" == "x0" ]] ; then
            echo -n "${i}s"
        else
            echo -n '.'
        fi
    done
    echo
}

scriptName=$(basename ${BASH_SOURCE[0]})
baseDir=/tmp/m102-${scriptName}

shardScript="/mnt/files/courses/M102-mongodb-for-dbas/chapter_6_scalability.2005166f6147/homework_6_1/week6.js"
if [[ ! -f ${shardScript} ]] ; then
    echoerr 'The expected shard script does not exist:' ${shardScript}
    exit
fi

displayHeader 'Cleaning up older runs of' ${scriptName}
pgrep --list-name -u $(whoami) mongos
pgrep --list-name -u $(whoami) mongod
pkill -u $(whoami) mongos
pkill -u $(whoami) mongod
rm -rfv ${baseDir}

displayHeader 'Sleeping for a bit to ensure old processes are really dead.'
sleeps 10

dbPath1=${baseDir}/db1
mkdir -p ${dbPath1}

configPath=${baseDir}/config
mkdir -p ${configPath}

logBase=${baseDir}/logs
mkdir -p ${logBase}
dbLogPath1=${logBase}/db1.log
configLogPath=${logBase}/config.log
shardLogPath=${logBase}/shard.log

dbPort1=27001
configPort=27010
shardPort=27020

bindIf=127.0.0.1
databaseName=week6
commonArgs="--logappend --fork --smallfiles --oplogSize 50 --bind_ip ${bindIf}"
displayHeader 'Forking the mongod processes for a sharded cluster.'
mongod ${commonArgs} --dbpath ${dbPath1} --logpath ${dbLogPath1} --port ${dbPort1}

displayHeader 'Sleeping for a minute to ensure the shard cluster comes online.'
sleeps 60

displayHeader 'Initializing replica set.'
time mongo --shell ${bindIf}:${dbPort1}/${databaseName} ${shardScript} <<EOF
    homework.init()
    db.trades.stats()
    exit
EOF

displayHeader 'Stopping the initialized mongod instance.'
pgrep --list-name -u $(whoami) mongod
pkill -u $(whoami) mongod

displayHeader 'Sleeping for a bit to ensure the instance is truly stopped.'
sleeps 10

displayHeader 'Starting the initialized mongod instance as a sharded server.'
mongod ${commonArgs} --dbpath ${dbPath1} --logpath ${dbLogPath1} --port ${dbPort1} --shardsvr

displayHeader 'Sleeping for a minute to ensure the sharded server comes online.'
sleeps 60

displayHeader 'Starting a config server.'
mongod ${commonArgs} --dbpath ${configPath} --logpath ${configLogPath} --port ${configPort} --configsvr

displayHeader 'Sleeping for a minute to ensure the config server comes online.'
sleeps 60

displayHeader 'Starting a shard database connected to the config server.'
mongos --logappend --fork --logpath ${shardLogPath} --port ${shardPort} --configdb ${bindIf}:${configPort}

displayHeader 'Sleep for a bit to ensure the config database comes online.'
sleeps 10

displayHeader 'Adding the first shard.'
mongo --shell ${bindIf}:${shardPort}/${databaseName} ${shardScript} <<EOF
    sh.addShard("${bindIf}:${dbPort1}")
    exit
EOF

displayHeader 'Sleep for a bit to ensure the first shard gets connected.'
sleeps 10

displayHeader 'Displaying some data since it should be visible by mongos.'
mongo --shell ${bindIf}:${shardPort}/${databaseName} ${shardScript} <<EOF
    db.trades.findOne()
    db.trades.count()
    db.trades.stats()

    print("!!ANSWER COMING!!")
    homework.a()
    print("!!END OF ANSWER!!")

    exit
EOF

displayHeader 'Cleaning up all of the damn resources.'
pgrep --list-name -u $(whoami) mongos
pgrep --list-name -u $(whoami) mongod
pkill -u $(whoami) mongos
pkill -u $(whoami) mongod
rm -rf ${basedir}

