#!/usr/bin/env bash

HEADER_COUNTER=0
function displayHeader() {
    HEADER_COUNTER=$((HEADER_COUNTER+1))
    echo
    echo '========================================'
    echo "${HEADER_COUNTER} => ${@}"
    echo
}

function echoerr() {
    echo 'ERROR:' $@ 1>&2
}

function sleeps() {
    seconds=$1
    echo -n "Sleeping for ${seconds} seconds "
    for i in $(seq 1 $seconds) ; do
        sleep 1
        if [[ "x$(( $i % 10))" == "x0" ]] ; then
            echo -n "${i}s"
        else
            echo -n '.'
        fi
    done
    echo
}

scriptName=$(basename ${BASH_SOURCE[0]})
baseDir=/tmp/m102-${scriptName}

shardScript="/mnt/files/courses/M102-mongodb-for-dbas/chapter_6_scalability.2005166f6147/homework_6_1/week6.js"
if [[ ! -f ${shardScript} ]] ; then
    echoerr 'The expected shard script does not exist:' ${shardScript}
    exit
fi

displayHeader 'Cleaning up older runs of' ${scriptName}
pgrep --list-name -u $(whoami) mongos
pgrep --list-name -u $(whoami) mongod
pkill -u $(whoami) mongos
pkill -u $(whoami) mongod
rm -rfv ${baseDir}

displayHeader 'Sleeping for a bit to ensure old processes are really dead.'
sleeps 10

dbPath1=${baseDir}/db1
dbPath2=${baseDir}/db2
mkdir -p ${dbPath1}
mkdir -p ${dbPath2}

configPath=${baseDir}/config
mkdir -p ${configPath}

logBase=${baseDir}/logs
mkdir -p ${logBase}
dbLogPath1=${logBase}/db1.log
dbLogPath2=${logBase}/db2.log
configLogPath=${logBase}/config.log
shardLogPath=${logBase}/shard.log

dbPort1=27001
dbPort2=27002
configPort=27010
shardPort=27020

bindIf=127.0.0.1
databaseName=week6
collectionName=trades
commonArgs="--logappend --fork --smallfiles --oplogSize 50 --bind_ip ${bindIf}"
displayHeader 'Starting a few servers to be a sharded cluster.'
mongod ${commonArgs} --dbpath ${dbPath1} --logpath ${dbLogPath1} --port ${dbPort1} --shardsvr
mongod ${commonArgs} --dbpath ${dbPath2} --logpath ${dbLogPath2} --port ${dbPort2} --shardsvr

displayHeader 'Sleeping for a minute to ensure the shard cluster comes online.'
sleeps 60

displayHeader 'Initializing first shard.'
time mongo --shell ${bindIf}:${dbPort1}/${databaseName} ${shardScript} <<EOF
    homework.init()
    db.trades.stats()
    exit
EOF

displayHeader 'Sleeping for a minute to ensure the sharded server comes online.'
sleeps 60

displayHeader 'Starting a config server.'
mongod ${commonArgs} --dbpath ${configPath} --logpath ${configLogPath} --port ${configPort} --configsvr

displayHeader 'Sleeping for a minute to ensure the config server comes online.'
sleeps 60

displayHeader 'Starting a shard database connected to the config server.'
mongos --logappend --fork --logpath ${shardLogPath} --port ${shardPort} --configdb ${bindIf}:${configPort}

displayHeader 'Sleep for a bit to ensure the config database comes online.'
sleeps 10

displayHeader 'Adding the first shard.'
mongo --shell ${bindIf}:${shardPort}/${databaseName} ${shardScript} <<EOF
    sh.addShard("${bindIf}:${dbPort1}")
    sh.addShard("${bindIf}:${dbPort2}")
    exit
EOF

displayHeader 'Sleep for a bit to ensure the first shard gets connected.'
sleeps 10

displayHeader 'Setting up the index and initializing the balancer.'
mongo --shell ${bindIf}:${shardPort}/${databaseName} ${shardScript} <<EOF
    sh.enableSharding("${databaseName}")
    var key = { ticker : 1, time : 1 }
    db.trades.createIndex(key)
    sh.shardCollection("${databaseName}.${collectionName}", key)
    exit
EOF

displayHeader 'Sleep for a bit to let the balancer do its thing.'
sleeps 60

displayHeader 'Ensuring the data was successfully sharded.'
mongo --shell ${bindIf}:${shardPort}/${databaseName} ${shardScript} <<EOF

    use config
    while(sh.isBalancerRunning()) {
        print("The balancer is still running...");
        sleep(1000);
    }

    var configdb = db.getSiblingDB("config")
    var partitioned = configdb.databases.findOne({"_id":"${databaseName}"}).partitioned;
    if (!partitioned) {
        throw "The ${databaseName} database is not partitioned!";
    }

    var count = configdb.chunks.find({ns:"week6.trades"}, {min:1,max:1,shard:1,_id:0}).sort({min:1}).count()
    if (count != 15) {
        throw "Expected 15 chunks, but found: " + count
    }

    exit
EOF

displayHeader 'Time to get the answer.'
mongo --shell ${bindIf}:${shardPort}/${databaseName} ${shardScript} <<EOF
    homework.check1()

    print("!!ANSWER COMING!!")
    homework.c()
    print("!!END OF ANSWER!!")

    exit
EOF

displayHeader 'Cleaning up all of the damn resources.'
pgrep --list-name -u $(whoami) mongos
pgrep --list-name -u $(whoami) mongod
pkill -u $(whoami) mongos
pkill -u $(whoami) mongod
rm -rf ${baseDir}

