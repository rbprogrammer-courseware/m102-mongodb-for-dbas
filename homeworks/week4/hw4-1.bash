#!/usr/bin/env bash

repscript="/mnt/files/courses/M102-mongodb-for-dbas/homework_4_1/replication/replication.js"
if [[ ! -f ${repscript} ]] ; then
    echo 'The expected replication script does not exist:' ${repscript}
    exit
fi

basedir=/tmp/m102-hw41
rm -rfv ${basedir}

dbpath1=${basedir}/db1
dbpath2=${basedir}/db2
dbpath3=${basedir}/db3
mkdir -p ${dbpath1}
mkdir -p ${dbpath2}
mkdir -p ${dbpath3}

logbase=${basedir}/logs
mkdir -p ${logbase}
logpath1=${logbase}/db1.log
logpath2=${logbase}/db2.log
logpath3=${logbase}/db3.log

port1=27001
port2=27002
port3=27003

commonargs="--logappend --fork --smallfiles --oplogSize 50"
mongod ${commonargs} --dbpath ${dbpath1} --logpath ${logpath1} --port ${port1} 
mongod ${commonargs} --dbpath ${dbpath2} --logpath ${logpath2} --port ${port2}
mongod ${commonargs} --dbpath ${dbpath3} --logpath ${logpath3} --port ${port3}

echo
echo
mongo --port ${port1} --shell ${repscript} <<EOF
    homework.init()
    print("!!ANSWER COMING!!")
    homework.a()
    print("!!END OF ANSWER!!")
    exit
EOF
echo
echo

pkill -u $(whoami) mongod
rm -rf ${basedir}

