#!/usr/bin/env bash

repscript="/mnt/files/courses/M102-mongodb-for-dbas/homework_4_1/replication/replication.js"
if [[ ! -f ${repscript} ]] ; then
    echo 'ERROR: The expected replication script does not exist:' ${repscript}
    exit
fi

HEADER_COUNTER=0
function displayHeader() {
    HEADER_COUNTER=$((HEADER_COUNTER+1))
    echo
    echo '========================================'
    echo "${HEADER_COUNTER} => ${@}"
    echo
}

basedir=/tmp/m102-hw44
rm -rfv ${basedir}

dbpath1=${basedir}/db1
dbpath2=${basedir}/db2
dbpath3=${basedir}/db3
mkdir -p ${dbpath1}
mkdir -p ${dbpath2}
mkdir -p ${dbpath3}

logbase=${basedir}/logs
mkdir -p ${logbase}
logpath1=${logbase}/db1.log
logpath2=${logbase}/db2.log
logpath3=${logbase}/db3.log

port1=27001
port2=27002
port3=27003

bindif=127.0.0.1
replSetName=rs0
commonargs="--logappend --fork --smallfiles --oplogSize 50 --replSet ${replSetName}"
#  --bind_ip ${bindif}
displayHeader 'Forking the mongod processes for a replica set.'
mongod ${commonargs} --dbpath ${dbpath1} --logpath ${logpath1} --port ${port1}
mongod ${commonargs} --dbpath ${dbpath2} --logpath ${logpath2} --port ${port2}
mongod ${commonargs} --dbpath ${dbpath3} --logpath ${logpath3} --port ${port3}

echo 'Sleep for a little bit to ensure the mongod processes come online.'
sleep 10

displayHeader 'Initializing replica set'
mongo --port ${port1} --shell ${repscript} <<EOF
    cfg = {
        _id : "${replSetName}",
        members : [
            { _id:0, host:"${bindif}:${port1}"},
            { _id:1, host:"${bindif}:${port2}"},
            { _id:2, host:"${bindif}:${port3}"}
        ]
    }
    rs.initiate(cfg)
    exit
EOF

echo 'Sleep for a minute to ensure the replica set comes online.'
sleep 1m

displayHeader 'Initializing homework.'
mongo --port ${port1} --shell ${repscript} <<EOF
    homework.init()
    exit
EOF

echo 'Sleep for a minute to ensure the homework initialization replicates.'
sleep 1m

displayHeader 'Stepping down the PRIMARY.'
mongo --port ${port1} --shell ${repscript} <<EOF
    rs.stepDown(120)
    exit
EOF

echo 'Sleep for a little bit to ensure the primary is brought down.'
sleep 10

displayHeader 'Terminating the PRIMARY process.'
processToKill=$(ps aux | grep mongod | grep ${port1} | grep -v grep | cut -d' ' -f 4)
if [[ ! -z "$processToKill" ]] ; then
    echo "Killing the primary node in the replica set ${replSetName}: ${processToKill}"
    kill ${processToKill}
    sleep 1
    kill -9 ${processToKill}
else
    echo "WTF?!  I could not determine PRIMARY's PID."
fi

displayHeader 'Determining who the newly elected PRIMARY is.'
isPrimary=$(mongo --port ${port2} --quiet --eval "d=db.isMaster(); print(d['ismaster']);")
if [[ "x${isPrimary}" == "xtrue" ]] ; then
    primaryPort=${port2}
else
    primaryPort=${port3}
fi

displayHeader 'Removing the old PRIMARY from the replica set configuration.'
mongo --port ${primaryPort} --shell ${repscript} <<EOF
    rs.status()
    rs.remove("${bindif}:${port1}")
    rs.status()
    exit
EOF

echo 'Sleep for a little bit to ensure the new primary comes online.'
sleep 10

echo
echo
displayHeader 'Answering the fucking homework question.'
mongo --port ${primaryPort} --shell ${repscript} <<EOF
    db.foo.find()
    print("!!ANSWER COMING!!")
    homework.d()
    print("!!END OF ANSWER!!")
    exit
EOF
echo
echo

displayHeader 'Cleaning up all of the damn resources.'
pkill -u $(whoami) mongod
pkill -9 -u $(whoami) mongod
rm -rf ${basedir}


