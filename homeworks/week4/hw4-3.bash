#!/usr/bin/env bash

repscript="/mnt/files/courses/M102-mongodb-for-dbas/homework_4_1/replication/replication.js"
if [[ ! -f ${repscript} ]] ; then
    echo 'The expected replication script does not exist:' ${repscript}
    exit
fi

basedir=/tmp/m102-hw43
rm -rfv ${basedir}

dbpath1=${basedir}/db1
dbpath2=${basedir}/db2
dbpath3=${basedir}/db3
mkdir -p ${dbpath1}
mkdir -p ${dbpath2}
mkdir -p ${dbpath3}

logbase=${basedir}/logs
mkdir -p ${logbase}
logpath1=${logbase}/db1.log
logpath2=${logbase}/db2.log
logpath3=${logbase}/db3.log

port1=27001
port2=27002
port3=27003

bindif=127.0.0.1
replSetName=rs0
commonargs="--logappend --fork --smallfiles --oplogSize 50 --replSet ${replSetName}"
#  --bind_ip ${bindif}
mongod ${commonargs} --dbpath ${dbpath1} --logpath ${logpath1} --port ${port1}
mongod ${commonargs} --dbpath ${dbpath2} --logpath ${logpath2} --port ${port2}
mongod ${commonargs} --dbpath ${dbpath3} --logpath ${logpath3} --port ${port3}

echo 'Sleep for a little bit to ensure the mongod processes come online.'
sleep 10

mongo --port ${port1} --shell ${repscript} <<EOF
    cfg = {
        _id : "${replSetName}",
        members : [
            { _id:0, host:"${bindif}:${port1}"},
            { _id:1, host:"${bindif}:${port2}"},
            { _id:2, host:"${bindif}:${port3}"}
        ]
    }
    rs.initiate(cfg)
    exit
EOF

echo 'Sleep for a little bit to ensure the replica set comes online.'
sleep 10

echo
echo
mongo --port ${port1} --shell ${repscript} <<EOF
    homework.init()
    db.foo.find()
    print("!!ANSWER COMING!!")
    homework.c()
    print("!!END OF ANSWER!!")
    exit
EOF
echo
echo

pkill -u $(whoami) mongod
pkill -9 -u $(whoami) mongod
rm -rf ${basedir}

