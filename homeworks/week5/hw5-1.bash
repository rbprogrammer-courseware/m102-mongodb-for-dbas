#!/usr/bin/env bash

HEADER_COUNTER=0
function displayHeader() {
    HEADER_COUNTER=$((HEADER_COUNTER+1))
    echo
    echo '========================================'
    echo "${HEADER_COUNTER} => ${@}"
    echo
}

scriptName=$(basename ${BASH_SOURCE[0]})
basedir=/tmp/m102-${scriptName}

displayHeader 'Cleaning up older runs of' ${scriptName}
pkill -u $(whoami) mongod
rm -rfv ${basedir}

displayHeader 'Sleeping for a bit to ensure old processes are really dead.'
sleep 10s

dbpath1=${basedir}/db1
dbpath2=${basedir}/db2
dbpath3=${basedir}/db3
mkdir -p ${dbpath1}
mkdir -p ${dbpath2}
mkdir -p ${dbpath3}

logbase=${basedir}/logs
mkdir -p ${logbase}
logpath1=${logbase}/db1.log
logpath2=${logbase}/db2.log
logpath3=${logbase}/db3.log

port1=27001
port2=27002
port3=27003

bindif=127.0.0.1
replSetName=rs0
commonargs="--logappend --fork --smallfiles --oplogSize 50 --replSet ${replSetName}"
displayHeader 'Forking the mongod processes for a replica set.'
mongod ${commonargs} --dbpath ${dbpath1} --logpath ${logpath1} --port ${port1} 
mongod ${commonargs} --dbpath ${dbpath2} --logpath ${logpath2} --port ${port2}
mongod ${commonargs} --dbpath ${dbpath3} --logpath ${logpath3} --port ${port3}

displayHeader 'Initializing replica set.'
mongo --port ${port1} <<EOF
    use local
    cfg = {
        _id : "${replSetName}",
        members : [
            { _id:0, host:"${bindif}:${port1}"},
            { _id:1, host:"${bindif}:${port2}"}
        ]
    }
    rs.initiate(cfg)
    exit
EOF

displayHeader 'Sleeping for a minute to ensure the replica set comes online.'
sleep 1m

displayHeader 'Adding the arbiter.'
mongo --port ${port1} <<EOF
    use local
    rs.slaveOk()
    rs.addArb("${bindif}:${port3}")
    rs.status()
    exit
EOF

displayHeader 'Sleeping for a minute to ensure the arbiter set comes online.'
sleep 1m

displayHeader 'Checking the status on the arbiter'
mongo --port ${port3} <<EOF
    use local
    var cfg = rs.status()
    print("!!ANSWER COMING!!")
    cfg.members[2].state
    print("!!END OF ANSWER!!")
    exit
EOF

displayHeader 'Cleaning up all of the damn resources.'
pkill -u $(whoami) mongod
rm -rf ${basedir}


