#!/bin/bash

dir=$1
if [[ "x${dir}" == "x" ]] ; then
    echo 'ERROR: Must specify a directory.' 1>&2
    exit $(false)
elif [[ ! -d "${dir}" ]] ; then
    echo 'ERROR: Argument must be an existent directory: ' ${dir} 1>&2
    exit $(false)
fi

pushd ${dir} &>/dev/null

find . -name ".DS_Store" | xargs rm -rfv
find . -name "__MACOSX" | xargs rm -rfv

popd &>/dev/null


