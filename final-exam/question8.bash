#!/usr/bin/env bash

HEADER_COUNTER=0
function displayHeader() {
    HEADER_COUNTER=$((HEADER_COUNTER+1))
    echo
    echo '========================================'
    echo "${HEADER_COUNTER} => ${@}"
    echo
}

function echoerr() {
    echo 'ERROR:' $@ 1>&2
}

function sleeps() {
    seconds=$1
    echo -n "Sleeping for ${seconds} seconds "
    for i in $(seq 1 $seconds) ; do
        sleep 1
        if [[ "x$(( $i % 10))" == "x0" ]] ; then
            echo -n "${i}s"
        else
            echo -n '.'
        fi
    done
    echo
}

function cleanup() {
    echo 'Killing Processes:'
    pgrep --list-name -u $(whoami) mongod
    pkill -u $(whoami) mongod
    echo 'Cleaning up directories:'
    rm -rfv ${baseDir}
    sleeps 5
}

scriptName=$(basename ${BASH_SOURCE[0]})
baseDir=/tmp/m102-final-${scriptName}

configBackup="/mnt/files/courses/M102-mongodb-for-dbas/final_exam.3de1e6ec52d1/question_8/gene_backup/gene_backup/config_server"
if [[ ! -d ${configBackup} ]] ; then
    echoerr 'The expected config backup does not exist:' ${configBackup}
    echoerr 'Cleaning up due to error.'
    cleanup
    exit
fi

s1Backup="/mnt/files/courses/M102-mongodb-for-dbas/final_exam.3de1e6ec52d1/question_8/gene_backup/gene_backup/s1"
if [[ ! -d ${s1Backup} ]] ; then
    echoerr 'The expected s1 backup does not exist:' ${s1Backup}
    echoerr 'Cleaning up due to error.'
    cleanup
    exit
fi

s2Backup="/mnt/files/courses/M102-mongodb-for-dbas/final_exam.3de1e6ec52d1/question_8/gene_backup/gene_backup/s1"
if [[ ! -d ${s2Backup} ]] ; then
    echoerr 'The expected s2 backup does not exist:' ${s2Backup}
    echoerr 'Cleaning up due to error.'
    cleanup
    exit
fi

displayHeader 'Cleaning up previous runs.'
cleanup

dbPath1=${baseDir}/s1
dbPath2=${baseDir}/s2
configPath=${baseDir}/config
mkdir -p ${dbPath1}
mkdir -p ${dbPath2}
mkdir -p ${configPath}

logBase=${baseDir}/logs
mkdir -p ${logBase}
dbLogPath1=${logBase}/s1.log
dbLogPath2=${logBase}/s2.log
configLogPath=${logBase}/config.log

dbPort1=27001
dbPort2=27002
configPort=27019

bindIf=127.0.0.1
replSetName=z
commonArgs="--logappend --fork --smallfiles --oplogSize 50 --bind_ip ${bindIf}"

displayHeader 'Starting a config server.'
mongod ${commonArgs} --dbpath ${configPath} --logpath ${configLogPath} --port ${configPort} --configsvr

displayHeader 'Waiting for the config server to come online.'
sleeps 10

displayHeader 'Restoring the config backup.'
mongorestore  --host ${bindIf}:${configPort} ${configBackup}

displayHeader 'Waiting for the config server to catch up with the restored data.'
sleeps 10

displayHeader 'Confirming the restoration of the config data.'
mongo --shell ${bindIf}:${configPort}/config <<EOF
    print("!!ANSWER COMING!!")
    db.chunks.find().sort({_id:1}).next().lastmodEpoch.getTimestamp().toUTCString().substr(20,6)
    print("!!END OF ANSWER!!")
    exit
EOF

displayHeader 'Cleaning up all of the damn resources.'
cleanup

