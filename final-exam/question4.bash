#!/usr/bin/env bash

HEADER_COUNTER=0
function displayHeader() {
    HEADER_COUNTER=$((HEADER_COUNTER+1))
    echo
    echo '========================================'
    echo "${HEADER_COUNTER} => ${@}"
    echo
}

function echoerr() {
    echo 'ERROR:' $@ 1>&2
}

function sleeps() {
    seconds=$1
    echo -n "Sleeping for ${seconds} seconds "
    for i in $(seq 1 $seconds) ; do
        sleep 1
        if [[ "x$(( $i % 10))" == "x0" ]] ; then
            echo -n "${i}s"
        else
            echo -n '.'
        fi
    done
    echo
}

function cleanup() {
    echo 'Killing Processes:'
    pgrep --list-name -u $(whoami) mongod
    pkill -u $(whoami) mongod
    echo 'Cleaning up directories:'
    rm -rfv ${baseDir}
    sleeps 5
}

scriptName=$(basename ${BASH_SOURCE[0]})
baseDir=/tmp/m102-final-${scriptName}

rollbackScript="/mnt/files/courses/M102-mongodb-for-dbas/final_exam.3de1e6ec52d1/question_1/rollback/a.js"
if [[ ! -f ${rollbackScript} ]] ; then
    echoerr 'The expected rollback script does not exist:' ${rollbackScript}
    echoerr 'Cleaning up due to error.'
    exit
fi

displayHeader 'Cleaning up previous runs.'
cleanup

dbPath1=${baseDir}/z1
dbPath2=${baseDir}/z2
dbPath3=${baseDir}/z3
mkdir -p ${dbPath1}
mkdir -p ${dbPath2}
mkdir -p ${dbPath3}

logBase=${baseDir}/logs
mkdir -p ${logBase}
dbLogPath1=${logBase}/z1.log
dbLogPath2=${logBase}/z2.log
dbLogPath3=${logBase}/z3.log

dbPort1=27001
dbPort2=27002
dbPort3=27003

bindIf=127.0.0.1
replSetName=z
commonArgs="--logappend --fork --smallfiles --oplogSize 50 --bind_ip ${bindIf} --replSet ${replSetName}"
displayHeader 'Starting a few servers to be a replicated cluster.'
mongod ${commonArgs} --logpath ${dbLogPath1} --port ${dbPort1} --dbpath ${dbPath1}
mongod ${commonArgs} --logpath ${dbLogPath2} --port ${dbPort2} --dbpath ${dbPath2}
mongod ${commonArgs} --logpath ${dbLogPath3} --port ${dbPort3} --dbpath ${dbPath3}

displayHeader 'Sleeping for a minute to ensure the replica set comes online.'
sleeps 60

displayHeader 'Initializing the replica set.'
mongo --shell ${bindIf}:${dbPort3} ${rollbackScript} <<EOF
    ourinit()
    exit
EOF

displayHeader 'Sleeping for a minute to ensure the replica set initializes.'
sleeps 60

displayHeader 'Determining who the PRIMARY is.'
isPrimary=$(mongo --port ${dbPort3} --quiet --eval "d=db.isMaster(); print(d['ismaster']);")
if [[ "x${isPrimary}" == "xtrue" ]] ; then
    echo "Mongo on ${dbPort3} is the primary."
else
    echoerr "Mongo on not ${dbPort3} is the primary, which was unexpected.  Just rerun this script!!!!"
    echoerr 'Cleaning up due to error.'
    cleanup
    exit
fi

displayHeader 'Setting up the initial data and stopping one of the replica sets.'
mongo --shell ${bindIf}:${dbPort3} ${rollbackScript} <<EOF
    db.foo.drop()
    db.foo.insert( { _id : 1 }, { writeConcern : { w : 2 } } )
    db.foo.insert( { _id : 2 }, { writeConcern : { w : 2 } } )
    db.foo.insert( { _id : 3 }, { writeConcern : { w : 2 } } )
    var a = connect("localhost:27001/admin");
    a.shutdownServer()
    rs.status()
    db.foo.insert( { _id : 4 } )
    db.foo.insert( { _id : 5 } )
    db.foo.insert( { _id : 6 } )
    exit
EOF

displayHeader 'Sleeping to ensure the single replica set initializes.'
sleeps 10

displayHeader 'Killing the last non-arbitor replica set.'
ps -A | grep mongod
ps ax | grep mongo | grep 27003 | awk '{print $1}' | xargs kill

displayHeader 'Sleeping to ensure the processes die.'
sleeps 10
extraProcs=$(pgrep -u $(whoami) mongod | wc -l)
if [[ "x${extraProcs}" == "x1" ]] ; then
    echo 'All extraneous non-arbitor mongod processes are dead.'
else
    echoerr 'Not all non-arbitor extraneous mongod processes are dead:' ${extraProcs}
    echoerr 'Cleaning up due to error.'
    cleanup
    exit
fi

displayHeader 'Starting the first node in the replica set.'
mongod ${commonArgs} --logpath ${dbLogPath1} --port ${dbPort1} --dbpath ${dbPath1}

displayHeader 'Waiting for the first replica set to come online.'
sleeps 10

displayHeader 'Adding a document into the first replica set.'
mongo --shell ${bindIf}:${dbPort1} ${rollbackScript} <<EOF
    db.foo.insert( { _id : "last" } )
    exit
EOF

displayHeader 'Starting the second node in the replica set.'
mongod ${commonArgs} --logpath ${dbLogPath3} --port ${dbPort3} --dbpath ${dbPath3}

displayHeader 'Waiting for the second replica set to come online.'
sleeps 10

displayHeader 'Getting the answer to the question.'
mongo --shell ${bindIf}:${dbPort3} ${rollbackScript} <<EOF
    // MongoDB does not permit the current primary to have a priority of 0, so
    // if this is the primary node then step down.
    rs.stepDown();

    // Set up a new configuration document.
    var cfg = { 
        _id:'z',
        members:[
            { _id:1, host:'${bindIf}:${dbPort1}' },
            { _id:2, host:'${bindIf}:${dbPort2}', "arbiterOnly" : true },
            { _id:3, host:'${bindIf}:${dbPort3}', "priority" : 0 }
        ]
    };

    // Reconfigure, but since we are now a secondary, force the replica set to
    // accept the new configuration
    rs.reconfig(cfg, { force: true });

    exit
EOF

displayHeader 'Waiting for the replica set to reconfigure itself.'
sleeps 60

mongo --shell ${bindIf}:${dbPort3} ${rollbackScript} <<EOF
    print("!!ANSWER COMING!!")
    part4()
    print("!!END OF ANSWER!!")
    exit
EOF

displayHeader 'Cleaning up all of the damn resources.'
cleanup

